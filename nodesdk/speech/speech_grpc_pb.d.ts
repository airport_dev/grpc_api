// GENERATED CODE -- DO NOT EDIT!

// package: speechpb
// file: speech.proto

import * as speech_pb from "./speech_pb";
import * as grpc from "grpc";

interface ISpeechService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
  setBeam: grpc.MethodDefinition<speech_pb.SetBeamRequest, speech_pb.SetBeamResponse>;
  getIat: grpc.MethodDefinition<speech_pb.GetIatRequest, speech_pb.GetIatResponse>;
  getNlp: grpc.MethodDefinition<speech_pb.GetNlpRequest, speech_pb.GetNlpResponse>;
  getTts: grpc.MethodDefinition<speech_pb.GetTtsRequest, speech_pb.GetTtsResponse>;
  onSpeechEvent: grpc.MethodDefinition<speech_pb.OnSpeechEventRequest, speech_pb.OnSpeechEventResponse>;
}

export const SpeechService: ISpeechService;

export class SpeechClient extends grpc.Client {
  constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
  setBeam(argument: speech_pb.SetBeamRequest, callback: grpc.requestCallback<speech_pb.SetBeamResponse>): grpc.ClientUnaryCall;
  setBeam(argument: speech_pb.SetBeamRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<speech_pb.SetBeamResponse>): grpc.ClientUnaryCall;
  setBeam(argument: speech_pb.SetBeamRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<speech_pb.SetBeamResponse>): grpc.ClientUnaryCall;
  getIat(argument: speech_pb.GetIatRequest, callback: grpc.requestCallback<speech_pb.GetIatResponse>): grpc.ClientUnaryCall;
  getIat(argument: speech_pb.GetIatRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<speech_pb.GetIatResponse>): grpc.ClientUnaryCall;
  getIat(argument: speech_pb.GetIatRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<speech_pb.GetIatResponse>): grpc.ClientUnaryCall;
  getNlp(argument: speech_pb.GetNlpRequest, callback: grpc.requestCallback<speech_pb.GetNlpResponse>): grpc.ClientUnaryCall;
  getNlp(argument: speech_pb.GetNlpRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<speech_pb.GetNlpResponse>): grpc.ClientUnaryCall;
  getNlp(argument: speech_pb.GetNlpRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<speech_pb.GetNlpResponse>): grpc.ClientUnaryCall;
  getTts(argument: speech_pb.GetTtsRequest, callback: grpc.requestCallback<speech_pb.GetTtsResponse>): grpc.ClientUnaryCall;
  getTts(argument: speech_pb.GetTtsRequest, metadataOrOptions: grpc.Metadata | grpc.CallOptions | null, callback: grpc.requestCallback<speech_pb.GetTtsResponse>): grpc.ClientUnaryCall;
  getTts(argument: speech_pb.GetTtsRequest, metadata: grpc.Metadata | null, options: grpc.CallOptions | null, callback: grpc.requestCallback<speech_pb.GetTtsResponse>): grpc.ClientUnaryCall;
  onSpeechEvent(metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientDuplexStream<speech_pb.OnSpeechEventRequest, speech_pb.OnSpeechEventResponse>;
  onSpeechEvent(metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientDuplexStream<speech_pb.OnSpeechEventRequest, speech_pb.OnSpeechEventResponse>;
}
