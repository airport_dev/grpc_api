// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var speech_pb = require('./speech_pb.js');

function serialize_speechpb_GetIatRequest(arg) {
  if (!(arg instanceof speech_pb.GetIatRequest)) {
    throw new Error('Expected argument of type speechpb.GetIatRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_GetIatRequest(buffer_arg) {
  return speech_pb.GetIatRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_speechpb_GetIatResponse(arg) {
  if (!(arg instanceof speech_pb.GetIatResponse)) {
    throw new Error('Expected argument of type speechpb.GetIatResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_GetIatResponse(buffer_arg) {
  return speech_pb.GetIatResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_speechpb_GetNlpRequest(arg) {
  if (!(arg instanceof speech_pb.GetNlpRequest)) {
    throw new Error('Expected argument of type speechpb.GetNlpRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_GetNlpRequest(buffer_arg) {
  return speech_pb.GetNlpRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_speechpb_GetNlpResponse(arg) {
  if (!(arg instanceof speech_pb.GetNlpResponse)) {
    throw new Error('Expected argument of type speechpb.GetNlpResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_GetNlpResponse(buffer_arg) {
  return speech_pb.GetNlpResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_speechpb_GetTtsRequest(arg) {
  if (!(arg instanceof speech_pb.GetTtsRequest)) {
    throw new Error('Expected argument of type speechpb.GetTtsRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_GetTtsRequest(buffer_arg) {
  return speech_pb.GetTtsRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_speechpb_GetTtsResponse(arg) {
  if (!(arg instanceof speech_pb.GetTtsResponse)) {
    throw new Error('Expected argument of type speechpb.GetTtsResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_GetTtsResponse(buffer_arg) {
  return speech_pb.GetTtsResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_speechpb_OnSpeechEventRequest(arg) {
  if (!(arg instanceof speech_pb.OnSpeechEventRequest)) {
    throw new Error('Expected argument of type speechpb.OnSpeechEventRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_OnSpeechEventRequest(buffer_arg) {
  return speech_pb.OnSpeechEventRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_speechpb_OnSpeechEventResponse(arg) {
  if (!(arg instanceof speech_pb.OnSpeechEventResponse)) {
    throw new Error('Expected argument of type speechpb.OnSpeechEventResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_OnSpeechEventResponse(buffer_arg) {
  return speech_pb.OnSpeechEventResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_speechpb_SetBeamRequest(arg) {
  if (!(arg instanceof speech_pb.SetBeamRequest)) {
    throw new Error('Expected argument of type speechpb.SetBeamRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_SetBeamRequest(buffer_arg) {
  return speech_pb.SetBeamRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_speechpb_SetBeamResponse(arg) {
  if (!(arg instanceof speech_pb.SetBeamResponse)) {
    throw new Error('Expected argument of type speechpb.SetBeamResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_speechpb_SetBeamResponse(buffer_arg) {
  return speech_pb.SetBeamResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var SpeechService = exports.SpeechService = {
  setBeam: {
    path: '/speechpb.Speech/SetBeam',
    requestStream: false,
    responseStream: false,
    requestType: speech_pb.SetBeamRequest,
    responseType: speech_pb.SetBeamResponse,
    requestSerialize: serialize_speechpb_SetBeamRequest,
    requestDeserialize: deserialize_speechpb_SetBeamRequest,
    responseSerialize: serialize_speechpb_SetBeamResponse,
    responseDeserialize: deserialize_speechpb_SetBeamResponse,
  },
  getIat: {
    path: '/speechpb.Speech/GetIat',
    requestStream: false,
    responseStream: false,
    requestType: speech_pb.GetIatRequest,
    responseType: speech_pb.GetIatResponse,
    requestSerialize: serialize_speechpb_GetIatRequest,
    requestDeserialize: deserialize_speechpb_GetIatRequest,
    responseSerialize: serialize_speechpb_GetIatResponse,
    responseDeserialize: deserialize_speechpb_GetIatResponse,
  },
  getNlp: {
    path: '/speechpb.Speech/GetNlp',
    requestStream: false,
    responseStream: false,
    requestType: speech_pb.GetNlpRequest,
    responseType: speech_pb.GetNlpResponse,
    requestSerialize: serialize_speechpb_GetNlpRequest,
    requestDeserialize: deserialize_speechpb_GetNlpRequest,
    responseSerialize: serialize_speechpb_GetNlpResponse,
    responseDeserialize: deserialize_speechpb_GetNlpResponse,
  },
  getTts: {
    path: '/speechpb.Speech/GetTts',
    requestStream: false,
    responseStream: false,
    requestType: speech_pb.GetTtsRequest,
    responseType: speech_pb.GetTtsResponse,
    requestSerialize: serialize_speechpb_GetTtsRequest,
    requestDeserialize: deserialize_speechpb_GetTtsRequest,
    responseSerialize: serialize_speechpb_GetTtsResponse,
    responseDeserialize: deserialize_speechpb_GetTtsResponse,
  },
  onSpeechEvent: {
    path: '/speechpb.Speech/OnSpeechEvent',
    requestStream: true,
    responseStream: true,
    requestType: speech_pb.OnSpeechEventRequest,
    responseType: speech_pb.OnSpeechEventResponse,
    requestSerialize: serialize_speechpb_OnSpeechEventRequest,
    requestDeserialize: deserialize_speechpb_OnSpeechEventRequest,
    responseSerialize: serialize_speechpb_OnSpeechEventResponse,
    responseDeserialize: deserialize_speechpb_OnSpeechEventResponse,
  },
};

exports.SpeechClient = grpc.makeGenericClientConstructor(SpeechService);
