// package: speechpb
// file: speech.proto

import * as jspb from "google-protobuf";

export class SetBeamRequest extends jspb.Message {
  getBeam(): number;
  setBeam(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetBeamRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SetBeamRequest): SetBeamRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetBeamRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetBeamRequest;
  static deserializeBinaryFromReader(message: SetBeamRequest, reader: jspb.BinaryReader): SetBeamRequest;
}

export namespace SetBeamRequest {
  export type AsObject = {
    beam: number,
  }
}

export class SetBeamResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SetBeamResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SetBeamResponse): SetBeamResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SetBeamResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SetBeamResponse;
  static deserializeBinaryFromReader(message: SetBeamResponse, reader: jspb.BinaryReader): SetBeamResponse;
}

export namespace SetBeamResponse {
  export type AsObject = {
  }
}

export class GetIatRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetIatRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetIatRequest): GetIatRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetIatRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetIatRequest;
  static deserializeBinaryFromReader(message: GetIatRequest, reader: jspb.BinaryReader): GetIatRequest;
}

export namespace GetIatRequest {
  export type AsObject = {
  }
}

export class GetIatResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getText(): string;
  setText(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetIatResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetIatResponse): GetIatResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetIatResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetIatResponse;
  static deserializeBinaryFromReader(message: GetIatResponse, reader: jspb.BinaryReader): GetIatResponse;
}

export namespace GetIatResponse {
  export type AsObject = {
    id: string,
    text: string,
  }
}

export class GetNlpRequest extends jspb.Message {
  getText(): string;
  setText(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetNlpRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetNlpRequest): GetNlpRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetNlpRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetNlpRequest;
  static deserializeBinaryFromReader(message: GetNlpRequest, reader: jspb.BinaryReader): GetNlpRequest;
}

export namespace GetNlpRequest {
  export type AsObject = {
    text: string,
  }
}

export class GetNlpResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getIntent(): Uint8Array | string;
  getIntent_asU8(): Uint8Array;
  getIntent_asB64(): string;
  setIntent(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetNlpResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetNlpResponse): GetNlpResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetNlpResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetNlpResponse;
  static deserializeBinaryFromReader(message: GetNlpResponse, reader: jspb.BinaryReader): GetNlpResponse;
}

export namespace GetNlpResponse {
  export type AsObject = {
    id: string,
    intent: Uint8Array | string,
  }
}

export class GetTtsRequest extends jspb.Message {
  getText(): string;
  setText(value: string): void;

  getVolume(): number;
  setVolume(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTtsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTtsRequest): GetTtsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTtsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTtsRequest;
  static deserializeBinaryFromReader(message: GetTtsRequest, reader: jspb.BinaryReader): GetTtsRequest;
}

export namespace GetTtsRequest {
  export type AsObject = {
    text: string,
    volume: number,
  }
}

export class GetTtsResponse extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getUrl(): string;
  setUrl(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTtsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetTtsResponse): GetTtsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTtsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTtsResponse;
  static deserializeBinaryFromReader(message: GetTtsResponse, reader: jspb.BinaryReader): GetTtsResponse;
}

export namespace GetTtsResponse {
  export type AsObject = {
    id: string,
    url: string,
  }
}

export class StartIat extends jspb.Message {
  getStart(): boolean;
  setStart(value: boolean): void;

  getWithNlp(): boolean;
  setWithNlp(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StartIat.AsObject;
  static toObject(includeInstance: boolean, msg: StartIat): StartIat.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StartIat, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StartIat;
  static deserializeBinaryFromReader(message: StartIat, reader: jspb.BinaryReader): StartIat;
}

export namespace StartIat {
  export type AsObject = {
    start: boolean,
    withNlp: boolean,
  }
}

export class StopIat extends jspb.Message {
  getStop(): boolean;
  setStop(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): StopIat.AsObject;
  static toObject(includeInstance: boolean, msg: StopIat): StopIat.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: StopIat, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): StopIat;
  static deserializeBinaryFromReader(message: StopIat, reader: jspb.BinaryReader): StopIat;
}

export namespace StopIat {
  export type AsObject = {
    stop: boolean,
  }
}

export class OnSpeechEventRequest extends jspb.Message {
  getStart(): boolean;
  setStart(value: boolean): void;

  getWithNlp(): boolean;
  setWithNlp(value: boolean): void;

  getStop(): boolean;
  setStop(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OnSpeechEventRequest.AsObject;
  static toObject(includeInstance: boolean, msg: OnSpeechEventRequest): OnSpeechEventRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OnSpeechEventRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OnSpeechEventRequest;
  static deserializeBinaryFromReader(message: OnSpeechEventRequest, reader: jspb.BinaryReader): OnSpeechEventRequest;
}

export namespace OnSpeechEventRequest {
  export type AsObject = {
    start: boolean,
    withNlp: boolean,
    stop: boolean,
  }
}

export class ErrorInfo extends jspb.Message {
  getInfo(): string;
  setInfo(value: string): void;

  getCode(): string;
  setCode(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ErrorInfo.AsObject;
  static toObject(includeInstance: boolean, msg: ErrorInfo): ErrorInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ErrorInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ErrorInfo;
  static deserializeBinaryFromReader(message: ErrorInfo, reader: jspb.BinaryReader): ErrorInfo;
}

export namespace ErrorInfo {
  export type AsObject = {
    info: string,
    code: string,
  }
}

export class Iat extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getText(): string;
  setText(value: string): void;

  getIsLast(): boolean;
  setIsLast(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Iat.AsObject;
  static toObject(includeInstance: boolean, msg: Iat): Iat.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Iat, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Iat;
  static deserializeBinaryFromReader(message: Iat, reader: jspb.BinaryReader): Iat;
}

export namespace Iat {
  export type AsObject = {
    id: string,
    text: string,
    isLast: boolean,
  }
}

export class Nlp extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getIntent(): Uint8Array | string;
  getIntent_asU8(): Uint8Array;
  getIntent_asB64(): string;
  setIntent(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Nlp.AsObject;
  static toObject(includeInstance: boolean, msg: Nlp): Nlp.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Nlp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Nlp;
  static deserializeBinaryFromReader(message: Nlp, reader: jspb.BinaryReader): Nlp;
}

export namespace Nlp {
  export type AsObject = {
    id: string,
    intent: Uint8Array | string,
  }
}

export class OnSpeechEventResponse extends jspb.Message {
  getEventType(): SpeechEventMap[keyof SpeechEventMap];
  setEventType(value: SpeechEventMap[keyof SpeechEventMap]): void;

  hasErrorInfo(): boolean;
  clearErrorInfo(): void;
  getErrorInfo(): ErrorInfo | undefined;
  setErrorInfo(value?: ErrorInfo): void;

  hasIat(): boolean;
  clearIat(): void;
  getIat(): Iat | undefined;
  setIat(value?: Iat): void;

  hasNlp(): boolean;
  clearNlp(): void;
  getNlp(): Nlp | undefined;
  setNlp(value?: Nlp): void;

  getEventOneofCase(): OnSpeechEventResponse.EventOneofCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OnSpeechEventResponse.AsObject;
  static toObject(includeInstance: boolean, msg: OnSpeechEventResponse): OnSpeechEventResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OnSpeechEventResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OnSpeechEventResponse;
  static deserializeBinaryFromReader(message: OnSpeechEventResponse, reader: jspb.BinaryReader): OnSpeechEventResponse;
}

export namespace OnSpeechEventResponse {
  export type AsObject = {
    eventType: SpeechEventMap[keyof SpeechEventMap],
    errorInfo?: ErrorInfo.AsObject,
    iat?: Iat.AsObject,
    nlp?: Nlp.AsObject,
  }

  export enum EventOneofCase {
    EVENT_ONEOF_NOT_SET = 0,
    ERROR_INFO = 2,
    IAT = 3,
    NLP = 4,
  }
}

export interface SpeechEventMap {
  UNKNOWN_EVENT: 0;
  ERROR: 1;
  IAT: 2;
  NLP: 3;
  TTS: 4;
}

export const SpeechEvent: SpeechEventMap;

