// package: twomicpb
// file: two_iat.proto

import * as jspb from "google-protobuf";

export class OnTwoMicSpeechEventRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OnTwoMicSpeechEventRequest.AsObject;
  static toObject(includeInstance: boolean, msg: OnTwoMicSpeechEventRequest): OnTwoMicSpeechEventRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OnTwoMicSpeechEventRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OnTwoMicSpeechEventRequest;
  static deserializeBinaryFromReader(message: OnTwoMicSpeechEventRequest, reader: jspb.BinaryReader): OnTwoMicSpeechEventRequest;
}

export namespace OnTwoMicSpeechEventRequest {
  export type AsObject = {
  }
}

export class ErrorInfo extends jspb.Message {
  getInfo(): string;
  setInfo(value: string): void;

  getCode(): string;
  setCode(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ErrorInfo.AsObject;
  static toObject(includeInstance: boolean, msg: ErrorInfo): ErrorInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ErrorInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ErrorInfo;
  static deserializeBinaryFromReader(message: ErrorInfo, reader: jspb.BinaryReader): ErrorInfo;
}

export namespace ErrorInfo {
  export type AsObject = {
    info: string,
    code: string,
  }
}

export class Iat extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getText(): string;
  setText(value: string): void;

  getSentenceComplete(): boolean;
  setSentenceComplete(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Iat.AsObject;
  static toObject(includeInstance: boolean, msg: Iat): Iat.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Iat, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Iat;
  static deserializeBinaryFromReader(message: Iat, reader: jspb.BinaryReader): Iat;
}

export namespace Iat {
  export type AsObject = {
    id: string,
    text: string,
    sentenceComplete: boolean,
  }
}

export class OnTwoMicSpeechEventResponse extends jspb.Message {
  getEventType(): TwoMicSpeechEventMap[keyof TwoMicSpeechEventMap];
  setEventType(value: TwoMicSpeechEventMap[keyof TwoMicSpeechEventMap]): void;

  hasErrorInfo(): boolean;
  clearErrorInfo(): void;
  getErrorInfo(): ErrorInfo | undefined;
  setErrorInfo(value?: ErrorInfo): void;

  hasIat1(): boolean;
  clearIat1(): void;
  getIat1(): Iat | undefined;
  setIat1(value?: Iat): void;

  hasIat2(): boolean;
  clearIat2(): void;
  getIat2(): Iat | undefined;
  setIat2(value?: Iat): void;

  getEventOneofCase(): OnTwoMicSpeechEventResponse.EventOneofCase;
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OnTwoMicSpeechEventResponse.AsObject;
  static toObject(includeInstance: boolean, msg: OnTwoMicSpeechEventResponse): OnTwoMicSpeechEventResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OnTwoMicSpeechEventResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OnTwoMicSpeechEventResponse;
  static deserializeBinaryFromReader(message: OnTwoMicSpeechEventResponse, reader: jspb.BinaryReader): OnTwoMicSpeechEventResponse;
}

export namespace OnTwoMicSpeechEventResponse {
  export type AsObject = {
    eventType: TwoMicSpeechEventMap[keyof TwoMicSpeechEventMap],
    errorInfo?: ErrorInfo.AsObject,
    iat1?: Iat.AsObject,
    iat2?: Iat.AsObject,
  }

  export enum EventOneofCase {
    EVENT_ONEOF_NOT_SET = 0,
    ERROR_INFO = 2,
    IAT1 = 3,
    IAT2 = 4,
  }
}

export interface TwoMicSpeechEventMap {
  MIC2_UNKNOWN_EVENT: 0;
  MIC2_ERROR: 1;
  MIC2_IAT1: 2;
  MIC2_IAT2: 3;
}

export const TwoMicSpeechEvent: TwoMicSpeechEventMap;

