// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var two_iat_pb = require('./two_iat_pb.js');

function serialize_twomicpb_OnTwoMicSpeechEventRequest(arg) {
  if (!(arg instanceof two_iat_pb.OnTwoMicSpeechEventRequest)) {
    throw new Error('Expected argument of type twomicpb.OnTwoMicSpeechEventRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_twomicpb_OnTwoMicSpeechEventRequest(buffer_arg) {
  return two_iat_pb.OnTwoMicSpeechEventRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_twomicpb_OnTwoMicSpeechEventResponse(arg) {
  if (!(arg instanceof two_iat_pb.OnTwoMicSpeechEventResponse)) {
    throw new Error('Expected argument of type twomicpb.OnTwoMicSpeechEventResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_twomicpb_OnTwoMicSpeechEventResponse(buffer_arg) {
  return two_iat_pb.OnTwoMicSpeechEventResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var TwoMicService = exports.TwoMicService = {
  onTwoMicEvent: {
    path: '/twomicpb.TwoMic/OnTwoMicEvent',
    requestStream: false,
    responseStream: true,
    requestType: two_iat_pb.OnTwoMicSpeechEventRequest,
    responseType: two_iat_pb.OnTwoMicSpeechEventResponse,
    requestSerialize: serialize_twomicpb_OnTwoMicSpeechEventRequest,
    requestDeserialize: deserialize_twomicpb_OnTwoMicSpeechEventRequest,
    responseSerialize: serialize_twomicpb_OnTwoMicSpeechEventResponse,
    responseDeserialize: deserialize_twomicpb_OnTwoMicSpeechEventResponse,
  },
};

exports.TwoMicClient = grpc.makeGenericClientConstructor(TwoMicService);
