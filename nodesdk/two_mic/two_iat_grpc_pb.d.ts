// GENERATED CODE -- DO NOT EDIT!

// package: twomicpb
// file: two_iat.proto

import * as two_iat_pb from "./two_iat_pb";
import * as grpc from "grpc";

interface ITwoMicService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
  onTwoMicEvent: grpc.MethodDefinition<two_iat_pb.OnTwoMicSpeechEventRequest, two_iat_pb.OnTwoMicSpeechEventResponse>;
}

export const TwoMicService: ITwoMicService;

export class TwoMicClient extends grpc.Client {
  constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
  onTwoMicEvent(argument: two_iat_pb.OnTwoMicSpeechEventRequest, metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientReadableStream<two_iat_pb.OnTwoMicSpeechEventResponse>;
  onTwoMicEvent(argument: two_iat_pb.OnTwoMicSpeechEventRequest, metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientReadableStream<two_iat_pb.OnTwoMicSpeechEventResponse>;
}
