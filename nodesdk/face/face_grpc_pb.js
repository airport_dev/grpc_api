// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var face_pb = require('./face_pb.js');

function serialize_face_OnFaceDetectRequest(arg) {
  if (!(arg instanceof face_pb.OnFaceDetectRequest)) {
    throw new Error('Expected argument of type face.OnFaceDetectRequest');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_face_OnFaceDetectRequest(buffer_arg) {
  return face_pb.OnFaceDetectRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_face_OnFaceDetectResponse(arg) {
  if (!(arg instanceof face_pb.OnFaceDetectResponse)) {
    throw new Error('Expected argument of type face.OnFaceDetectResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_face_OnFaceDetectResponse(buffer_arg) {
  return face_pb.OnFaceDetectResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var FaceService = exports.FaceService = {
  onFaceDetect: {
    path: '/face.Face/OnFaceDetect',
    requestStream: false,
    responseStream: true,
    requestType: face_pb.OnFaceDetectRequest,
    responseType: face_pb.OnFaceDetectResponse,
    requestSerialize: serialize_face_OnFaceDetectRequest,
    requestDeserialize: deserialize_face_OnFaceDetectRequest,
    responseSerialize: serialize_face_OnFaceDetectResponse,
    responseDeserialize: deserialize_face_OnFaceDetectResponse,
  },
};

exports.FaceClient = grpc.makeGenericClientConstructor(FaceService);
