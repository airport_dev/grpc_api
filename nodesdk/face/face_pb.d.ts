// package: face
// file: face.proto

import * as jspb from "google-protobuf";

export class OnFaceDetectRequest extends jspb.Message {
  getWithImg(): boolean;
  setWithImg(value: boolean): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OnFaceDetectRequest.AsObject;
  static toObject(includeInstance: boolean, msg: OnFaceDetectRequest): OnFaceDetectRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OnFaceDetectRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OnFaceDetectRequest;
  static deserializeBinaryFromReader(message: OnFaceDetectRequest, reader: jspb.BinaryReader): OnFaceDetectRequest;
}

export namespace OnFaceDetectRequest {
  export type AsObject = {
    withImg: boolean,
  }
}

export class FaceRect extends jspb.Message {
  getLeft(): number;
  setLeft(value: number): void;

  getTop(): number;
  setTop(value: number): void;

  getRight(): number;
  setRight(value: number): void;

  getBottom(): number;
  setBottom(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FaceRect.AsObject;
  static toObject(includeInstance: boolean, msg: FaceRect): FaceRect.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FaceRect, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FaceRect;
  static deserializeBinaryFromReader(message: FaceRect, reader: jspb.BinaryReader): FaceRect;
}

export namespace FaceRect {
  export type AsObject = {
    left: number,
    top: number,
    right: number,
    bottom: number,
  }
}

export class OnFaceDetectResponse extends jspb.Message {
  clearFaceRectsList(): void;
  getFaceRectsList(): Array<FaceRect>;
  setFaceRectsList(value: Array<FaceRect>): void;
  addFaceRects(value?: FaceRect, index?: number): FaceRect;

  getImg(): Uint8Array | string;
  getImg_asU8(): Uint8Array;
  getImg_asB64(): string;
  setImg(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OnFaceDetectResponse.AsObject;
  static toObject(includeInstance: boolean, msg: OnFaceDetectResponse): OnFaceDetectResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OnFaceDetectResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OnFaceDetectResponse;
  static deserializeBinaryFromReader(message: OnFaceDetectResponse, reader: jspb.BinaryReader): OnFaceDetectResponse;
}

export namespace OnFaceDetectResponse {
  export type AsObject = {
    faceRectsList: Array<FaceRect.AsObject>,
    img: Uint8Array | string,
  }
}

