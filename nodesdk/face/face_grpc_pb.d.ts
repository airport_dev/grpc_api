// GENERATED CODE -- DO NOT EDIT!

// package: face
// file: face.proto

import * as face_pb from "./face_pb";
import * as grpc from "grpc";

interface IFaceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
  onFaceDetect: grpc.MethodDefinition<face_pb.OnFaceDetectRequest, face_pb.OnFaceDetectResponse>;
}

export const FaceService: IFaceService;

export class FaceClient extends grpc.Client {
  constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
  onFaceDetect(argument: face_pb.OnFaceDetectRequest, metadataOrOptions?: grpc.Metadata | grpc.CallOptions | null): grpc.ClientReadableStream<face_pb.OnFaceDetectResponse>;
  onFaceDetect(argument: face_pb.OnFaceDetectRequest, metadata?: grpc.Metadata | null, options?: grpc.CallOptions | null): grpc.ClientReadableStream<face_pb.OnFaceDetectResponse>;
}
