// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.23.0
// 	protoc        v3.14.0
// source: proto/two_mic/two_mic.proto

package twomicpb

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

//TwoMic事件
type TwoMicSpeechEvent int32

const (
	TwoMicSpeechEvent_Mic2_UNKNOWN_EVENT TwoMicSpeechEvent = 0 // 零值
	TwoMicSpeechEvent_Mic2_ERROR         TwoMicSpeechEvent = 1 // 识别错误
	TwoMicSpeechEvent_Mic2_IAT1          TwoMicSpeechEvent = 2 //麦1识别结果
	TwoMicSpeechEvent_Mic2_IAT2          TwoMicSpeechEvent = 3 //麦2识别结果
)

// Enum value maps for TwoMicSpeechEvent.
var (
	TwoMicSpeechEvent_name = map[int32]string{
		0: "Mic2_UNKNOWN_EVENT",
		1: "Mic2_ERROR",
		2: "Mic2_IAT1",
		3: "Mic2_IAT2",
	}
	TwoMicSpeechEvent_value = map[string]int32{
		"Mic2_UNKNOWN_EVENT": 0,
		"Mic2_ERROR":         1,
		"Mic2_IAT1":          2,
		"Mic2_IAT2":          3,
	}
)

func (x TwoMicSpeechEvent) Enum() *TwoMicSpeechEvent {
	p := new(TwoMicSpeechEvent)
	*p = x
	return p
}

func (x TwoMicSpeechEvent) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (TwoMicSpeechEvent) Descriptor() protoreflect.EnumDescriptor {
	return file_proto_two_mic_two_mic_proto_enumTypes[0].Descriptor()
}

func (TwoMicSpeechEvent) Type() protoreflect.EnumType {
	return &file_proto_two_mic_two_mic_proto_enumTypes[0]
}

func (x TwoMicSpeechEvent) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use TwoMicSpeechEvent.Descriptor instead.
func (TwoMicSpeechEvent) EnumDescriptor() ([]byte, []int) {
	return file_proto_two_mic_two_mic_proto_rawDescGZIP(), []int{0}
}

type OnTwoMicSpeechEventRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields
}

func (x *OnTwoMicSpeechEventRequest) Reset() {
	*x = OnTwoMicSpeechEventRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_two_mic_two_mic_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *OnTwoMicSpeechEventRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*OnTwoMicSpeechEventRequest) ProtoMessage() {}

func (x *OnTwoMicSpeechEventRequest) ProtoReflect() protoreflect.Message {
	mi := &file_proto_two_mic_two_mic_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use OnTwoMicSpeechEventRequest.ProtoReflect.Descriptor instead.
func (*OnTwoMicSpeechEventRequest) Descriptor() ([]byte, []int) {
	return file_proto_two_mic_two_mic_proto_rawDescGZIP(), []int{0}
}

//错误信息
type ErrorInfo struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Info string `protobuf:"bytes,1,opt,name=info,proto3" json:"info,omitempty"` // 错误描述（设备提供，可为空）
	Code string `protobuf:"bytes,2,opt,name=code,proto3" json:"code,omitempty"` // 错误码（设备提供，可为空）
}

func (x *ErrorInfo) Reset() {
	*x = ErrorInfo{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_two_mic_two_mic_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ErrorInfo) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ErrorInfo) ProtoMessage() {}

func (x *ErrorInfo) ProtoReflect() protoreflect.Message {
	mi := &file_proto_two_mic_two_mic_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ErrorInfo.ProtoReflect.Descriptor instead.
func (*ErrorInfo) Descriptor() ([]byte, []int) {
	return file_proto_two_mic_two_mic_proto_rawDescGZIP(), []int{1}
}

func (x *ErrorInfo) GetInfo() string {
	if x != nil {
		return x.Info
	}
	return ""
}

func (x *ErrorInfo) GetCode() string {
	if x != nil {
		return x.Code
	}
	return ""
}

//识别结果
type Iat struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id               string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`                                                      //当前回话id
	Text             string `protobuf:"bytes,2,opt,name=text,proto3" json:"text,omitempty"`                                                  //音频转文本内容
	SentenceComplete bool   `protobuf:"varint,3,opt,name=sentence_complete,json=sentenceComplete,proto3" json:"sentence_complete,omitempty"` // 是否为完整句
}

func (x *Iat) Reset() {
	*x = Iat{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_two_mic_two_mic_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Iat) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Iat) ProtoMessage() {}

func (x *Iat) ProtoReflect() protoreflect.Message {
	mi := &file_proto_two_mic_two_mic_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Iat.ProtoReflect.Descriptor instead.
func (*Iat) Descriptor() ([]byte, []int) {
	return file_proto_two_mic_two_mic_proto_rawDescGZIP(), []int{2}
}

func (x *Iat) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Iat) GetText() string {
	if x != nil {
		return x.Text
	}
	return ""
}

func (x *Iat) GetSentenceComplete() bool {
	if x != nil {
		return x.SentenceComplete
	}
	return false
}

type OnTwoMicSpeechEventResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	EventType TwoMicSpeechEvent `protobuf:"varint,1,opt,name=event_type,json=eventType,proto3,enum=twomicpb.TwoMicSpeechEvent" json:"event_type,omitempty"`
	// Types that are assignable to EventOneof:
	//	*OnTwoMicSpeechEventResponse_ErrorInfo
	//	*OnTwoMicSpeechEventResponse_Iat1
	//	*OnTwoMicSpeechEventResponse_Iat2
	EventOneof isOnTwoMicSpeechEventResponse_EventOneof `protobuf_oneof:"event_oneof"`
}

func (x *OnTwoMicSpeechEventResponse) Reset() {
	*x = OnTwoMicSpeechEventResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_proto_two_mic_two_mic_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *OnTwoMicSpeechEventResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*OnTwoMicSpeechEventResponse) ProtoMessage() {}

func (x *OnTwoMicSpeechEventResponse) ProtoReflect() protoreflect.Message {
	mi := &file_proto_two_mic_two_mic_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use OnTwoMicSpeechEventResponse.ProtoReflect.Descriptor instead.
func (*OnTwoMicSpeechEventResponse) Descriptor() ([]byte, []int) {
	return file_proto_two_mic_two_mic_proto_rawDescGZIP(), []int{3}
}

func (x *OnTwoMicSpeechEventResponse) GetEventType() TwoMicSpeechEvent {
	if x != nil {
		return x.EventType
	}
	return TwoMicSpeechEvent_Mic2_UNKNOWN_EVENT
}

func (m *OnTwoMicSpeechEventResponse) GetEventOneof() isOnTwoMicSpeechEventResponse_EventOneof {
	if m != nil {
		return m.EventOneof
	}
	return nil
}

func (x *OnTwoMicSpeechEventResponse) GetErrorInfo() *ErrorInfo {
	if x, ok := x.GetEventOneof().(*OnTwoMicSpeechEventResponse_ErrorInfo); ok {
		return x.ErrorInfo
	}
	return nil
}

func (x *OnTwoMicSpeechEventResponse) GetIat1() *Iat {
	if x, ok := x.GetEventOneof().(*OnTwoMicSpeechEventResponse_Iat1); ok {
		return x.Iat1
	}
	return nil
}

func (x *OnTwoMicSpeechEventResponse) GetIat2() *Iat {
	if x, ok := x.GetEventOneof().(*OnTwoMicSpeechEventResponse_Iat2); ok {
		return x.Iat2
	}
	return nil
}

type isOnTwoMicSpeechEventResponse_EventOneof interface {
	isOnTwoMicSpeechEventResponse_EventOneof()
}

type OnTwoMicSpeechEventResponse_ErrorInfo struct {
	ErrorInfo *ErrorInfo `protobuf:"bytes,2,opt,name=error_info,json=errorInfo,proto3,oneof"` // 错误
}

type OnTwoMicSpeechEventResponse_Iat1 struct {
	Iat1 *Iat `protobuf:"bytes,3,opt,name=iat1,proto3,oneof"` // 麦1 asr识别结果
}

type OnTwoMicSpeechEventResponse_Iat2 struct {
	Iat2 *Iat `protobuf:"bytes,4,opt,name=iat2,proto3,oneof"` //麦2 asr识别结果
}

func (*OnTwoMicSpeechEventResponse_ErrorInfo) isOnTwoMicSpeechEventResponse_EventOneof() {}

func (*OnTwoMicSpeechEventResponse_Iat1) isOnTwoMicSpeechEventResponse_EventOneof() {}

func (*OnTwoMicSpeechEventResponse_Iat2) isOnTwoMicSpeechEventResponse_EventOneof() {}

var File_proto_two_mic_two_mic_proto protoreflect.FileDescriptor

var file_proto_two_mic_two_mic_proto_rawDesc = []byte{
	0x0a, 0x1b, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x74, 0x77, 0x6f, 0x5f, 0x6d, 0x69, 0x63, 0x2f,
	0x74, 0x77, 0x6f, 0x5f, 0x6d, 0x69, 0x63, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x08, 0x74,
	0x77, 0x6f, 0x6d, 0x69, 0x63, 0x70, 0x62, 0x22, 0x1c, 0x0a, 0x1a, 0x4f, 0x6e, 0x54, 0x77, 0x6f,
	0x4d, 0x69, 0x63, 0x53, 0x70, 0x65, 0x65, 0x63, 0x68, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x22, 0x33, 0x0a, 0x09, 0x45, 0x72, 0x72, 0x6f, 0x72, 0x49, 0x6e,
	0x66, 0x6f, 0x12, 0x12, 0x0a, 0x04, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x04, 0x69, 0x6e, 0x66, 0x6f, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x63, 0x6f, 0x64, 0x65, 0x22, 0x56, 0x0a, 0x03, 0x49, 0x61,
	0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69,
	0x64, 0x12, 0x12, 0x0a, 0x04, 0x74, 0x65, 0x78, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x74, 0x65, 0x78, 0x74, 0x12, 0x2b, 0x0a, 0x11, 0x73, 0x65, 0x6e, 0x74, 0x65, 0x6e, 0x63,
	0x65, 0x5f, 0x63, 0x6f, 0x6d, 0x70, 0x6c, 0x65, 0x74, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x08,
	0x52, 0x10, 0x73, 0x65, 0x6e, 0x74, 0x65, 0x6e, 0x63, 0x65, 0x43, 0x6f, 0x6d, 0x70, 0x6c, 0x65,
	0x74, 0x65, 0x22, 0xe8, 0x01, 0x0a, 0x1b, 0x4f, 0x6e, 0x54, 0x77, 0x6f, 0x4d, 0x69, 0x63, 0x53,
	0x70, 0x65, 0x65, 0x63, 0x68, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x3a, 0x0a, 0x0a, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x5f, 0x74, 0x79, 0x70, 0x65,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x1b, 0x2e, 0x74, 0x77, 0x6f, 0x6d, 0x69, 0x63, 0x70,
	0x62, 0x2e, 0x54, 0x77, 0x6f, 0x4d, 0x69, 0x63, 0x53, 0x70, 0x65, 0x65, 0x63, 0x68, 0x45, 0x76,
	0x65, 0x6e, 0x74, 0x52, 0x09, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x34,
	0x0a, 0x0a, 0x65, 0x72, 0x72, 0x6f, 0x72, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x13, 0x2e, 0x74, 0x77, 0x6f, 0x6d, 0x69, 0x63, 0x70, 0x62, 0x2e, 0x45, 0x72,
	0x72, 0x6f, 0x72, 0x49, 0x6e, 0x66, 0x6f, 0x48, 0x00, 0x52, 0x09, 0x65, 0x72, 0x72, 0x6f, 0x72,
	0x49, 0x6e, 0x66, 0x6f, 0x12, 0x23, 0x0a, 0x04, 0x69, 0x61, 0x74, 0x31, 0x18, 0x03, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x0d, 0x2e, 0x74, 0x77, 0x6f, 0x6d, 0x69, 0x63, 0x70, 0x62, 0x2e, 0x49, 0x61,
	0x74, 0x48, 0x00, 0x52, 0x04, 0x69, 0x61, 0x74, 0x31, 0x12, 0x23, 0x0a, 0x04, 0x69, 0x61, 0x74,
	0x32, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x0d, 0x2e, 0x74, 0x77, 0x6f, 0x6d, 0x69, 0x63,
	0x70, 0x62, 0x2e, 0x49, 0x61, 0x74, 0x48, 0x00, 0x52, 0x04, 0x69, 0x61, 0x74, 0x32, 0x42, 0x0d,
	0x0a, 0x0b, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x5f, 0x6f, 0x6e, 0x65, 0x6f, 0x66, 0x2a, 0x59, 0x0a,
	0x11, 0x54, 0x77, 0x6f, 0x4d, 0x69, 0x63, 0x53, 0x70, 0x65, 0x65, 0x63, 0x68, 0x45, 0x76, 0x65,
	0x6e, 0x74, 0x12, 0x16, 0x0a, 0x12, 0x4d, 0x69, 0x63, 0x32, 0x5f, 0x55, 0x4e, 0x4b, 0x4e, 0x4f,
	0x57, 0x4e, 0x5f, 0x45, 0x56, 0x45, 0x4e, 0x54, 0x10, 0x00, 0x12, 0x0e, 0x0a, 0x0a, 0x4d, 0x69,
	0x63, 0x32, 0x5f, 0x45, 0x52, 0x52, 0x4f, 0x52, 0x10, 0x01, 0x12, 0x0d, 0x0a, 0x09, 0x4d, 0x69,
	0x63, 0x32, 0x5f, 0x49, 0x41, 0x54, 0x31, 0x10, 0x02, 0x12, 0x0d, 0x0a, 0x09, 0x4d, 0x69, 0x63,
	0x32, 0x5f, 0x49, 0x41, 0x54, 0x32, 0x10, 0x03, 0x32, 0x6a, 0x0a, 0x06, 0x54, 0x77, 0x6f, 0x4d,
	0x69, 0x63, 0x12, 0x60, 0x0a, 0x0d, 0x4f, 0x6e, 0x54, 0x77, 0x6f, 0x4d, 0x69, 0x63, 0x45, 0x76,
	0x65, 0x6e, 0x74, 0x12, 0x24, 0x2e, 0x74, 0x77, 0x6f, 0x6d, 0x69, 0x63, 0x70, 0x62, 0x2e, 0x4f,
	0x6e, 0x54, 0x77, 0x6f, 0x4d, 0x69, 0x63, 0x53, 0x70, 0x65, 0x65, 0x63, 0x68, 0x45, 0x76, 0x65,
	0x6e, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x25, 0x2e, 0x74, 0x77, 0x6f, 0x6d,
	0x69, 0x63, 0x70, 0x62, 0x2e, 0x4f, 0x6e, 0x54, 0x77, 0x6f, 0x4d, 0x69, 0x63, 0x53, 0x70, 0x65,
	0x65, 0x63, 0x68, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x22, 0x00, 0x30, 0x01, 0x42, 0x0c, 0x5a, 0x0a, 0x2e, 0x3b, 0x74, 0x77, 0x6f, 0x6d, 0x69, 0x63,
	0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_proto_two_mic_two_mic_proto_rawDescOnce sync.Once
	file_proto_two_mic_two_mic_proto_rawDescData = file_proto_two_mic_two_mic_proto_rawDesc
)

func file_proto_two_mic_two_mic_proto_rawDescGZIP() []byte {
	file_proto_two_mic_two_mic_proto_rawDescOnce.Do(func() {
		file_proto_two_mic_two_mic_proto_rawDescData = protoimpl.X.CompressGZIP(file_proto_two_mic_two_mic_proto_rawDescData)
	})
	return file_proto_two_mic_two_mic_proto_rawDescData
}

var file_proto_two_mic_two_mic_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_proto_two_mic_two_mic_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_proto_two_mic_two_mic_proto_goTypes = []interface{}{
	(TwoMicSpeechEvent)(0),              // 0: twomicpb.TwoMicSpeechEvent
	(*OnTwoMicSpeechEventRequest)(nil),  // 1: twomicpb.OnTwoMicSpeechEventRequest
	(*ErrorInfo)(nil),                   // 2: twomicpb.ErrorInfo
	(*Iat)(nil),                         // 3: twomicpb.Iat
	(*OnTwoMicSpeechEventResponse)(nil), // 4: twomicpb.OnTwoMicSpeechEventResponse
}
var file_proto_two_mic_two_mic_proto_depIdxs = []int32{
	0, // 0: twomicpb.OnTwoMicSpeechEventResponse.event_type:type_name -> twomicpb.TwoMicSpeechEvent
	2, // 1: twomicpb.OnTwoMicSpeechEventResponse.error_info:type_name -> twomicpb.ErrorInfo
	3, // 2: twomicpb.OnTwoMicSpeechEventResponse.iat1:type_name -> twomicpb.Iat
	3, // 3: twomicpb.OnTwoMicSpeechEventResponse.iat2:type_name -> twomicpb.Iat
	1, // 4: twomicpb.TwoMic.OnTwoMicEvent:input_type -> twomicpb.OnTwoMicSpeechEventRequest
	4, // 5: twomicpb.TwoMic.OnTwoMicEvent:output_type -> twomicpb.OnTwoMicSpeechEventResponse
	5, // [5:6] is the sub-list for method output_type
	4, // [4:5] is the sub-list for method input_type
	4, // [4:4] is the sub-list for extension type_name
	4, // [4:4] is the sub-list for extension extendee
	0, // [0:4] is the sub-list for field type_name
}

func init() { file_proto_two_mic_two_mic_proto_init() }
func file_proto_two_mic_two_mic_proto_init() {
	if File_proto_two_mic_two_mic_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_proto_two_mic_two_mic_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*OnTwoMicSpeechEventRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_two_mic_two_mic_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ErrorInfo); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_two_mic_two_mic_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Iat); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_proto_two_mic_two_mic_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*OnTwoMicSpeechEventResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_proto_two_mic_two_mic_proto_msgTypes[3].OneofWrappers = []interface{}{
		(*OnTwoMicSpeechEventResponse_ErrorInfo)(nil),
		(*OnTwoMicSpeechEventResponse_Iat1)(nil),
		(*OnTwoMicSpeechEventResponse_Iat2)(nil),
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_proto_two_mic_two_mic_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_proto_two_mic_two_mic_proto_goTypes,
		DependencyIndexes: file_proto_two_mic_two_mic_proto_depIdxs,
		EnumInfos:         file_proto_two_mic_two_mic_proto_enumTypes,
		MessageInfos:      file_proto_two_mic_two_mic_proto_msgTypes,
	}.Build()
	File_proto_two_mic_two_mic_proto = out.File
	file_proto_two_mic_two_mic_proto_rawDesc = nil
	file_proto_two_mic_two_mic_proto_goTypes = nil
	file_proto_two_mic_two_mic_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// TwoMicClient is the client API for TwoMic service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type TwoMicClient interface {
	OnTwoMicEvent(ctx context.Context, in *OnTwoMicSpeechEventRequest, opts ...grpc.CallOption) (TwoMic_OnTwoMicEventClient, error)
}

type twoMicClient struct {
	cc grpc.ClientConnInterface
}

func NewTwoMicClient(cc grpc.ClientConnInterface) TwoMicClient {
	return &twoMicClient{cc}
}

func (c *twoMicClient) OnTwoMicEvent(ctx context.Context, in *OnTwoMicSpeechEventRequest, opts ...grpc.CallOption) (TwoMic_OnTwoMicEventClient, error) {
	stream, err := c.cc.NewStream(ctx, &_TwoMic_serviceDesc.Streams[0], "/twomicpb.TwoMic/OnTwoMicEvent", opts...)
	if err != nil {
		return nil, err
	}
	x := &twoMicOnTwoMicEventClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type TwoMic_OnTwoMicEventClient interface {
	Recv() (*OnTwoMicSpeechEventResponse, error)
	grpc.ClientStream
}

type twoMicOnTwoMicEventClient struct {
	grpc.ClientStream
}

func (x *twoMicOnTwoMicEventClient) Recv() (*OnTwoMicSpeechEventResponse, error) {
	m := new(OnTwoMicSpeechEventResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// TwoMicServer is the server API for TwoMic service.
type TwoMicServer interface {
	OnTwoMicEvent(*OnTwoMicSpeechEventRequest, TwoMic_OnTwoMicEventServer) error
}

// UnimplementedTwoMicServer can be embedded to have forward compatible implementations.
type UnimplementedTwoMicServer struct {
}

func (*UnimplementedTwoMicServer) OnTwoMicEvent(*OnTwoMicSpeechEventRequest, TwoMic_OnTwoMicEventServer) error {
	return status.Errorf(codes.Unimplemented, "method OnTwoMicEvent not implemented")
}

func RegisterTwoMicServer(s *grpc.Server, srv TwoMicServer) {
	s.RegisterService(&_TwoMic_serviceDesc, srv)
}

func _TwoMic_OnTwoMicEvent_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(OnTwoMicSpeechEventRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(TwoMicServer).OnTwoMicEvent(m, &twoMicOnTwoMicEventServer{stream})
}

type TwoMic_OnTwoMicEventServer interface {
	Send(*OnTwoMicSpeechEventResponse) error
	grpc.ServerStream
}

type twoMicOnTwoMicEventServer struct {
	grpc.ServerStream
}

func (x *twoMicOnTwoMicEventServer) Send(m *OnTwoMicSpeechEventResponse) error {
	return x.ServerStream.SendMsg(m)
}

var _TwoMic_serviceDesc = grpc.ServiceDesc{
	ServiceName: "twomicpb.TwoMic",
	HandlerType: (*TwoMicServer)(nil),
	Methods:     []grpc.MethodDesc{},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "OnTwoMicEvent",
			Handler:       _TwoMic_OnTwoMicEvent_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "proto/two_mic/two_mic.proto",
}
