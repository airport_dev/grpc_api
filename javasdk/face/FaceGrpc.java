package face;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.29.1-SNAPSHOT)",
    comments = "Source: face.proto")
public final class FaceGrpc {

  private FaceGrpc() {}

  public static final String SERVICE_NAME = "face.Face";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<face.FaceOuterClass.OnFaceDetectRequest,
      face.FaceOuterClass.OnFaceDetectResponse> getOnFaceDetectMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "OnFaceDetect",
      requestType = face.FaceOuterClass.OnFaceDetectRequest.class,
      responseType = face.FaceOuterClass.OnFaceDetectResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<face.FaceOuterClass.OnFaceDetectRequest,
      face.FaceOuterClass.OnFaceDetectResponse> getOnFaceDetectMethod() {
    io.grpc.MethodDescriptor<face.FaceOuterClass.OnFaceDetectRequest, face.FaceOuterClass.OnFaceDetectResponse> getOnFaceDetectMethod;
    if ((getOnFaceDetectMethod = FaceGrpc.getOnFaceDetectMethod) == null) {
      synchronized (FaceGrpc.class) {
        if ((getOnFaceDetectMethod = FaceGrpc.getOnFaceDetectMethod) == null) {
          FaceGrpc.getOnFaceDetectMethod = getOnFaceDetectMethod =
              io.grpc.MethodDescriptor.<face.FaceOuterClass.OnFaceDetectRequest, face.FaceOuterClass.OnFaceDetectResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "OnFaceDetect"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  face.FaceOuterClass.OnFaceDetectRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  face.FaceOuterClass.OnFaceDetectResponse.getDefaultInstance()))
              .setSchemaDescriptor(new FaceMethodDescriptorSupplier("OnFaceDetect"))
              .build();
        }
      }
    }
    return getOnFaceDetectMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static FaceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<FaceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<FaceStub>() {
        @java.lang.Override
        public FaceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new FaceStub(channel, callOptions);
        }
      };
    return FaceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static FaceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<FaceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<FaceBlockingStub>() {
        @java.lang.Override
        public FaceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new FaceBlockingStub(channel, callOptions);
        }
      };
    return FaceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static FaceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<FaceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<FaceFutureStub>() {
        @java.lang.Override
        public FaceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new FaceFutureStub(channel, callOptions);
        }
      };
    return FaceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class FaceImplBase implements io.grpc.BindableService {

    /**
     */
    public void onFaceDetect(face.FaceOuterClass.OnFaceDetectRequest request,
        io.grpc.stub.StreamObserver<face.FaceOuterClass.OnFaceDetectResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getOnFaceDetectMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getOnFaceDetectMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                face.FaceOuterClass.OnFaceDetectRequest,
                face.FaceOuterClass.OnFaceDetectResponse>(
                  this, METHODID_ON_FACE_DETECT)))
          .build();
    }
  }

  /**
   */
  public static final class FaceStub extends io.grpc.stub.AbstractAsyncStub<FaceStub> {
    private FaceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new FaceStub(channel, callOptions);
    }

    /**
     */
    public void onFaceDetect(face.FaceOuterClass.OnFaceDetectRequest request,
        io.grpc.stub.StreamObserver<face.FaceOuterClass.OnFaceDetectResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getOnFaceDetectMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class FaceBlockingStub extends io.grpc.stub.AbstractBlockingStub<FaceBlockingStub> {
    private FaceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new FaceBlockingStub(channel, callOptions);
    }

    /**
     */
    public java.util.Iterator<face.FaceOuterClass.OnFaceDetectResponse> onFaceDetect(
        face.FaceOuterClass.OnFaceDetectRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getOnFaceDetectMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class FaceFutureStub extends io.grpc.stub.AbstractFutureStub<FaceFutureStub> {
    private FaceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected FaceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new FaceFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_ON_FACE_DETECT = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final FaceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(FaceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ON_FACE_DETECT:
          serviceImpl.onFaceDetect((face.FaceOuterClass.OnFaceDetectRequest) request,
              (io.grpc.stub.StreamObserver<face.FaceOuterClass.OnFaceDetectResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class FaceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    FaceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return face.FaceOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Face");
    }
  }

  private static final class FaceFileDescriptorSupplier
      extends FaceBaseDescriptorSupplier {
    FaceFileDescriptorSupplier() {}
  }

  private static final class FaceMethodDescriptorSupplier
      extends FaceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    FaceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (FaceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new FaceFileDescriptorSupplier())
              .addMethod(getOnFaceDetectMethod())
              .build();
        }
      }
    }
    return result;
  }
}
