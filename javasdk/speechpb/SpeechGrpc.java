package speechpb;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.29.1-SNAPSHOT)",
    comments = "Source: speech.proto")
public final class SpeechGrpc {

  private SpeechGrpc() {}

  public static final String SERVICE_NAME = "speechpb.Speech";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.SetBeamRequest,
      speechpb.SpeechOuterClass.SetBeamResponse> getSetBeamMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetBeam",
      requestType = speechpb.SpeechOuterClass.SetBeamRequest.class,
      responseType = speechpb.SpeechOuterClass.SetBeamResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.SetBeamRequest,
      speechpb.SpeechOuterClass.SetBeamResponse> getSetBeamMethod() {
    io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.SetBeamRequest, speechpb.SpeechOuterClass.SetBeamResponse> getSetBeamMethod;
    if ((getSetBeamMethod = SpeechGrpc.getSetBeamMethod) == null) {
      synchronized (SpeechGrpc.class) {
        if ((getSetBeamMethod = SpeechGrpc.getSetBeamMethod) == null) {
          SpeechGrpc.getSetBeamMethod = getSetBeamMethod =
              io.grpc.MethodDescriptor.<speechpb.SpeechOuterClass.SetBeamRequest, speechpb.SpeechOuterClass.SetBeamResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SetBeam"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.SetBeamRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.SetBeamResponse.getDefaultInstance()))
              .setSchemaDescriptor(new SpeechMethodDescriptorSupplier("SetBeam"))
              .build();
        }
      }
    }
    return getSetBeamMethod;
  }

  private static volatile io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.GetIatRequest,
      speechpb.SpeechOuterClass.GetIatResponse> getGetIatMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetIat",
      requestType = speechpb.SpeechOuterClass.GetIatRequest.class,
      responseType = speechpb.SpeechOuterClass.GetIatResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.GetIatRequest,
      speechpb.SpeechOuterClass.GetIatResponse> getGetIatMethod() {
    io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.GetIatRequest, speechpb.SpeechOuterClass.GetIatResponse> getGetIatMethod;
    if ((getGetIatMethod = SpeechGrpc.getGetIatMethod) == null) {
      synchronized (SpeechGrpc.class) {
        if ((getGetIatMethod = SpeechGrpc.getGetIatMethod) == null) {
          SpeechGrpc.getGetIatMethod = getGetIatMethod =
              io.grpc.MethodDescriptor.<speechpb.SpeechOuterClass.GetIatRequest, speechpb.SpeechOuterClass.GetIatResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetIat"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.GetIatRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.GetIatResponse.getDefaultInstance()))
              .setSchemaDescriptor(new SpeechMethodDescriptorSupplier("GetIat"))
              .build();
        }
      }
    }
    return getGetIatMethod;
  }

  private static volatile io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.GetNlpRequest,
      speechpb.SpeechOuterClass.GetNlpResponse> getGetNlpMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetNlp",
      requestType = speechpb.SpeechOuterClass.GetNlpRequest.class,
      responseType = speechpb.SpeechOuterClass.GetNlpResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.GetNlpRequest,
      speechpb.SpeechOuterClass.GetNlpResponse> getGetNlpMethod() {
    io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.GetNlpRequest, speechpb.SpeechOuterClass.GetNlpResponse> getGetNlpMethod;
    if ((getGetNlpMethod = SpeechGrpc.getGetNlpMethod) == null) {
      synchronized (SpeechGrpc.class) {
        if ((getGetNlpMethod = SpeechGrpc.getGetNlpMethod) == null) {
          SpeechGrpc.getGetNlpMethod = getGetNlpMethod =
              io.grpc.MethodDescriptor.<speechpb.SpeechOuterClass.GetNlpRequest, speechpb.SpeechOuterClass.GetNlpResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetNlp"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.GetNlpRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.GetNlpResponse.getDefaultInstance()))
              .setSchemaDescriptor(new SpeechMethodDescriptorSupplier("GetNlp"))
              .build();
        }
      }
    }
    return getGetNlpMethod;
  }

  private static volatile io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.GetTtsRequest,
      speechpb.SpeechOuterClass.GetTtsResponse> getGetTtsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetTts",
      requestType = speechpb.SpeechOuterClass.GetTtsRequest.class,
      responseType = speechpb.SpeechOuterClass.GetTtsResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.GetTtsRequest,
      speechpb.SpeechOuterClass.GetTtsResponse> getGetTtsMethod() {
    io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.GetTtsRequest, speechpb.SpeechOuterClass.GetTtsResponse> getGetTtsMethod;
    if ((getGetTtsMethod = SpeechGrpc.getGetTtsMethod) == null) {
      synchronized (SpeechGrpc.class) {
        if ((getGetTtsMethod = SpeechGrpc.getGetTtsMethod) == null) {
          SpeechGrpc.getGetTtsMethod = getGetTtsMethod =
              io.grpc.MethodDescriptor.<speechpb.SpeechOuterClass.GetTtsRequest, speechpb.SpeechOuterClass.GetTtsResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetTts"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.GetTtsRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.GetTtsResponse.getDefaultInstance()))
              .setSchemaDescriptor(new SpeechMethodDescriptorSupplier("GetTts"))
              .build();
        }
      }
    }
    return getGetTtsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.OnSpeechEventRequest,
      speechpb.SpeechOuterClass.OnSpeechEventResponse> getOnSpeechEventMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "OnSpeechEvent",
      requestType = speechpb.SpeechOuterClass.OnSpeechEventRequest.class,
      responseType = speechpb.SpeechOuterClass.OnSpeechEventResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.OnSpeechEventRequest,
      speechpb.SpeechOuterClass.OnSpeechEventResponse> getOnSpeechEventMethod() {
    io.grpc.MethodDescriptor<speechpb.SpeechOuterClass.OnSpeechEventRequest, speechpb.SpeechOuterClass.OnSpeechEventResponse> getOnSpeechEventMethod;
    if ((getOnSpeechEventMethod = SpeechGrpc.getOnSpeechEventMethod) == null) {
      synchronized (SpeechGrpc.class) {
        if ((getOnSpeechEventMethod = SpeechGrpc.getOnSpeechEventMethod) == null) {
          SpeechGrpc.getOnSpeechEventMethod = getOnSpeechEventMethod =
              io.grpc.MethodDescriptor.<speechpb.SpeechOuterClass.OnSpeechEventRequest, speechpb.SpeechOuterClass.OnSpeechEventResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "OnSpeechEvent"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.OnSpeechEventRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  speechpb.SpeechOuterClass.OnSpeechEventResponse.getDefaultInstance()))
              .setSchemaDescriptor(new SpeechMethodDescriptorSupplier("OnSpeechEvent"))
              .build();
        }
      }
    }
    return getOnSpeechEventMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SpeechStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SpeechStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SpeechStub>() {
        @java.lang.Override
        public SpeechStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SpeechStub(channel, callOptions);
        }
      };
    return SpeechStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SpeechBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SpeechBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SpeechBlockingStub>() {
        @java.lang.Override
        public SpeechBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SpeechBlockingStub(channel, callOptions);
        }
      };
    return SpeechBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SpeechFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SpeechFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SpeechFutureStub>() {
        @java.lang.Override
        public SpeechFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SpeechFutureStub(channel, callOptions);
        }
      };
    return SpeechFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class SpeechImplBase implements io.grpc.BindableService {

    /**
     */
    public void setBeam(speechpb.SpeechOuterClass.SetBeamRequest request,
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.SetBeamResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getSetBeamMethod(), responseObserver);
    }

    /**
     */
    public void getIat(speechpb.SpeechOuterClass.GetIatRequest request,
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.GetIatResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetIatMethod(), responseObserver);
    }

    /**
     */
    public void getNlp(speechpb.SpeechOuterClass.GetNlpRequest request,
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.GetNlpResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetNlpMethod(), responseObserver);
    }

    /**
     */
    public void getTts(speechpb.SpeechOuterClass.GetTtsRequest request,
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.GetTtsResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetTtsMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.OnSpeechEventRequest> onSpeechEvent(
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.OnSpeechEventResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(getOnSpeechEventMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSetBeamMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                speechpb.SpeechOuterClass.SetBeamRequest,
                speechpb.SpeechOuterClass.SetBeamResponse>(
                  this, METHODID_SET_BEAM)))
          .addMethod(
            getGetIatMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                speechpb.SpeechOuterClass.GetIatRequest,
                speechpb.SpeechOuterClass.GetIatResponse>(
                  this, METHODID_GET_IAT)))
          .addMethod(
            getGetNlpMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                speechpb.SpeechOuterClass.GetNlpRequest,
                speechpb.SpeechOuterClass.GetNlpResponse>(
                  this, METHODID_GET_NLP)))
          .addMethod(
            getGetTtsMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                speechpb.SpeechOuterClass.GetTtsRequest,
                speechpb.SpeechOuterClass.GetTtsResponse>(
                  this, METHODID_GET_TTS)))
          .addMethod(
            getOnSpeechEventMethod(),
            asyncBidiStreamingCall(
              new MethodHandlers<
                speechpb.SpeechOuterClass.OnSpeechEventRequest,
                speechpb.SpeechOuterClass.OnSpeechEventResponse>(
                  this, METHODID_ON_SPEECH_EVENT)))
          .build();
    }
  }

  /**
   */
  public static final class SpeechStub extends io.grpc.stub.AbstractAsyncStub<SpeechStub> {
    private SpeechStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SpeechStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SpeechStub(channel, callOptions);
    }

    /**
     */
    public void setBeam(speechpb.SpeechOuterClass.SetBeamRequest request,
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.SetBeamResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getSetBeamMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getIat(speechpb.SpeechOuterClass.GetIatRequest request,
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.GetIatResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetIatMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getNlp(speechpb.SpeechOuterClass.GetNlpRequest request,
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.GetNlpResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetNlpMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getTts(speechpb.SpeechOuterClass.GetTtsRequest request,
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.GetTtsResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetTtsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.OnSpeechEventRequest> onSpeechEvent(
        io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.OnSpeechEventResponse> responseObserver) {
      return asyncBidiStreamingCall(
          getChannel().newCall(getOnSpeechEventMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   */
  public static final class SpeechBlockingStub extends io.grpc.stub.AbstractBlockingStub<SpeechBlockingStub> {
    private SpeechBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SpeechBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SpeechBlockingStub(channel, callOptions);
    }

    /**
     */
    public speechpb.SpeechOuterClass.SetBeamResponse setBeam(speechpb.SpeechOuterClass.SetBeamRequest request) {
      return blockingUnaryCall(
          getChannel(), getSetBeamMethod(), getCallOptions(), request);
    }

    /**
     */
    public speechpb.SpeechOuterClass.GetIatResponse getIat(speechpb.SpeechOuterClass.GetIatRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetIatMethod(), getCallOptions(), request);
    }

    /**
     */
    public speechpb.SpeechOuterClass.GetNlpResponse getNlp(speechpb.SpeechOuterClass.GetNlpRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetNlpMethod(), getCallOptions(), request);
    }

    /**
     */
    public speechpb.SpeechOuterClass.GetTtsResponse getTts(speechpb.SpeechOuterClass.GetTtsRequest request) {
      return blockingUnaryCall(
          getChannel(), getGetTtsMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class SpeechFutureStub extends io.grpc.stub.AbstractFutureStub<SpeechFutureStub> {
    private SpeechFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SpeechFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SpeechFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<speechpb.SpeechOuterClass.SetBeamResponse> setBeam(
        speechpb.SpeechOuterClass.SetBeamRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getSetBeamMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<speechpb.SpeechOuterClass.GetIatResponse> getIat(
        speechpb.SpeechOuterClass.GetIatRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetIatMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<speechpb.SpeechOuterClass.GetNlpResponse> getNlp(
        speechpb.SpeechOuterClass.GetNlpRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetNlpMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<speechpb.SpeechOuterClass.GetTtsResponse> getTts(
        speechpb.SpeechOuterClass.GetTtsRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getGetTtsMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SET_BEAM = 0;
  private static final int METHODID_GET_IAT = 1;
  private static final int METHODID_GET_NLP = 2;
  private static final int METHODID_GET_TTS = 3;
  private static final int METHODID_ON_SPEECH_EVENT = 4;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SpeechImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SpeechImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SET_BEAM:
          serviceImpl.setBeam((speechpb.SpeechOuterClass.SetBeamRequest) request,
              (io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.SetBeamResponse>) responseObserver);
          break;
        case METHODID_GET_IAT:
          serviceImpl.getIat((speechpb.SpeechOuterClass.GetIatRequest) request,
              (io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.GetIatResponse>) responseObserver);
          break;
        case METHODID_GET_NLP:
          serviceImpl.getNlp((speechpb.SpeechOuterClass.GetNlpRequest) request,
              (io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.GetNlpResponse>) responseObserver);
          break;
        case METHODID_GET_TTS:
          serviceImpl.getTts((speechpb.SpeechOuterClass.GetTtsRequest) request,
              (io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.GetTtsResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ON_SPEECH_EVENT:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.onSpeechEvent(
              (io.grpc.stub.StreamObserver<speechpb.SpeechOuterClass.OnSpeechEventResponse>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SpeechBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SpeechBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return speechpb.SpeechOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Speech");
    }
  }

  private static final class SpeechFileDescriptorSupplier
      extends SpeechBaseDescriptorSupplier {
    SpeechFileDescriptorSupplier() {}
  }

  private static final class SpeechMethodDescriptorSupplier
      extends SpeechBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SpeechMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SpeechGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SpeechFileDescriptorSupplier())
              .addMethod(getSetBeamMethod())
              .addMethod(getGetIatMethod())
              .addMethod(getGetNlpMethod())
              .addMethod(getGetTtsMethod())
              .addMethod(getOnSpeechEventMethod())
              .build();
        }
      }
    }
    return result;
  }
}
