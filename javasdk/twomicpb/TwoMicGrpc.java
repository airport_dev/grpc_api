package twomicpb;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.29.1-SNAPSHOT)",
    comments = "Source: two_iat.proto")
public final class TwoMicGrpc {

  private TwoMicGrpc() {}

  public static final String SERVICE_NAME = "twomicpb.TwoMic";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<twomicpb.TwoIat.OnTwoMicSpeechEventRequest,
      twomicpb.TwoIat.OnTwoMicSpeechEventResponse> getOnTwoMicEventMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "OnTwoMicEvent",
      requestType = twomicpb.TwoIat.OnTwoMicSpeechEventRequest.class,
      responseType = twomicpb.TwoIat.OnTwoMicSpeechEventResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<twomicpb.TwoIat.OnTwoMicSpeechEventRequest,
      twomicpb.TwoIat.OnTwoMicSpeechEventResponse> getOnTwoMicEventMethod() {
    io.grpc.MethodDescriptor<twomicpb.TwoIat.OnTwoMicSpeechEventRequest, twomicpb.TwoIat.OnTwoMicSpeechEventResponse> getOnTwoMicEventMethod;
    if ((getOnTwoMicEventMethod = TwoMicGrpc.getOnTwoMicEventMethod) == null) {
      synchronized (TwoMicGrpc.class) {
        if ((getOnTwoMicEventMethod = TwoMicGrpc.getOnTwoMicEventMethod) == null) {
          TwoMicGrpc.getOnTwoMicEventMethod = getOnTwoMicEventMethod =
              io.grpc.MethodDescriptor.<twomicpb.TwoIat.OnTwoMicSpeechEventRequest, twomicpb.TwoIat.OnTwoMicSpeechEventResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "OnTwoMicEvent"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  twomicpb.TwoIat.OnTwoMicSpeechEventRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  twomicpb.TwoIat.OnTwoMicSpeechEventResponse.getDefaultInstance()))
              .setSchemaDescriptor(new TwoMicMethodDescriptorSupplier("OnTwoMicEvent"))
              .build();
        }
      }
    }
    return getOnTwoMicEventMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static TwoMicStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<TwoMicStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<TwoMicStub>() {
        @java.lang.Override
        public TwoMicStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new TwoMicStub(channel, callOptions);
        }
      };
    return TwoMicStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static TwoMicBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<TwoMicBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<TwoMicBlockingStub>() {
        @java.lang.Override
        public TwoMicBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new TwoMicBlockingStub(channel, callOptions);
        }
      };
    return TwoMicBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static TwoMicFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<TwoMicFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<TwoMicFutureStub>() {
        @java.lang.Override
        public TwoMicFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new TwoMicFutureStub(channel, callOptions);
        }
      };
    return TwoMicFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class TwoMicImplBase implements io.grpc.BindableService {

    /**
     */
    public void onTwoMicEvent(twomicpb.TwoIat.OnTwoMicSpeechEventRequest request,
        io.grpc.stub.StreamObserver<twomicpb.TwoIat.OnTwoMicSpeechEventResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getOnTwoMicEventMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getOnTwoMicEventMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                twomicpb.TwoIat.OnTwoMicSpeechEventRequest,
                twomicpb.TwoIat.OnTwoMicSpeechEventResponse>(
                  this, METHODID_ON_TWO_MIC_EVENT)))
          .build();
    }
  }

  /**
   */
  public static final class TwoMicStub extends io.grpc.stub.AbstractAsyncStub<TwoMicStub> {
    private TwoMicStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TwoMicStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new TwoMicStub(channel, callOptions);
    }

    /**
     */
    public void onTwoMicEvent(twomicpb.TwoIat.OnTwoMicSpeechEventRequest request,
        io.grpc.stub.StreamObserver<twomicpb.TwoIat.OnTwoMicSpeechEventResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getOnTwoMicEventMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class TwoMicBlockingStub extends io.grpc.stub.AbstractBlockingStub<TwoMicBlockingStub> {
    private TwoMicBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TwoMicBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new TwoMicBlockingStub(channel, callOptions);
    }

    /**
     */
    public java.util.Iterator<twomicpb.TwoIat.OnTwoMicSpeechEventResponse> onTwoMicEvent(
        twomicpb.TwoIat.OnTwoMicSpeechEventRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getOnTwoMicEventMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class TwoMicFutureStub extends io.grpc.stub.AbstractFutureStub<TwoMicFutureStub> {
    private TwoMicFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected TwoMicFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new TwoMicFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_ON_TWO_MIC_EVENT = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final TwoMicImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(TwoMicImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ON_TWO_MIC_EVENT:
          serviceImpl.onTwoMicEvent((twomicpb.TwoIat.OnTwoMicSpeechEventRequest) request,
              (io.grpc.stub.StreamObserver<twomicpb.TwoIat.OnTwoMicSpeechEventResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class TwoMicBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    TwoMicBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return twomicpb.TwoIat.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("TwoMic");
    }
  }

  private static final class TwoMicFileDescriptorSupplier
      extends TwoMicBaseDescriptorSupplier {
    TwoMicFileDescriptorSupplier() {}
  }

  private static final class TwoMicMethodDescriptorSupplier
      extends TwoMicBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    TwoMicMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (TwoMicGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new TwoMicFileDescriptorSupplier())
              .addMethod(getOnTwoMicEventMethod())
              .build();
        }
      }
    }
    return result;
  }
}
